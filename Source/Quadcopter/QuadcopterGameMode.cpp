// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Quadcopter.h"
#include "QuadcopterGameMode.h"
#include "QuadcopterPawn.h"

AQuadcopterGameMode::AQuadcopterGameMode()
{
	// set default pawn class to our flying pawn
	DefaultPawnClass = AQuadcopterPawn::StaticClass();
}
