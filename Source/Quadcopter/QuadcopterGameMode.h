// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "QuadcopterGameMode.generated.h"

UCLASS(minimalapi)
class AQuadcopterGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AQuadcopterGameMode();
};



