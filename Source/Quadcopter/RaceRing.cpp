// Fill out your copyright notice in the Description page of Project Settings.

#include "Quadcopter.h"
#include "RaceRing.h"


// Sets default values
ARaceRing::ARaceRing(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	// Add Subcomponents
	MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Sphere"));
	RootComponent = MeshComponent;


}

// Called when the game starts or when spawned
void ARaceRing::BeginPlay()
{
	Super::BeginPlay();
	// set up a notification for when this Actor starts overlapping another Actor  
	OnActorBeginOverlap.AddDynamic(this, &ARaceRing::OnOverlapBegin);
}

// Called every frame
void ARaceRing::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ARaceRing::OnOverlapBegin(class AActor* OtherActor)
{
	if (OtherActor->GetClass() == AQuadcopterPawn::StaticClass())
		Destroy();
}