// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "QuadcopterPawn.h"
#include "RaceRing.generated.h"

UCLASS()
class QUADCOPTER_API ARaceRing : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARaceRing(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Callback function
	UFUNCTION( BlueprintCallable, Category = "Example Nodes")
	void OnOverlapBegin(class AActor* OtherActor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* RingModel;
private:
	UMeshComponent* MeshComponent;
};
