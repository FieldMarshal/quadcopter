#pragma once

#include "Quadcopter.h"
#include "SpiderSensor/hidapi.h"
#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include "hidapi.h"
#include <iostream>
#include "math.h"
#include <chrono>

class sensorData
{
private:
	float rot[2];
	float omega[2];
	float angle[2];
public:
	sensorData();
	float getGyroDelta(float gyro);
	float deadzone(float value);
	int LabelG(float axisValue, int multiplier);
	float twoDecimal(float value);
	int LabelXL(float axisValue, int multiplier);
	void detectGesture(float data[]);
	float cleanData(float value);
	int getData(unsigned char buf[], int sensor_data[], float final_data[], hid_device *handle);
	double sensorData::getAngle(int dps, double gyro);

};